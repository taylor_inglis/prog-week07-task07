﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_7
{
    class Program
    {
        static void Main(string[] args)
        {
            obtainingInput();
            obtainingInput();
            obtainingInput();
            obtainingInput();
            obtainingInput();
        }
        static int calculations(int a, int b, int c, int d, int e)
        {
            var output = a + b + c + d + e;
            return output;
        }
        static void obtainingInput()
        {
            Console.WriteLine("Please enter 5 numbers");
            var a = int.Parse(Console.ReadLine());
            var b = int.Parse(Console.ReadLine());
            var c = int.Parse(Console.ReadLine());
            var d = int.Parse(Console.ReadLine());
            var e = int.Parse(Console.ReadLine());
            var output = calculations(a, b, c, d, e);
            Console.WriteLine(output);
        }
    }
}
